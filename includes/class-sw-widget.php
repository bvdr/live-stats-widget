<?php
/**
 * StatsWidget display widget
 *
 * @package StatsWidget
 */

defined( 'ABSPATH' ) || exit;

/**
 * StatsWidget Display.
 *
 * @class StatsWidget/widget
 */
class SW_Widget extends WP_Widget {
	/**
	 * StatsWidget Constructor.
	 */
	public function __construct() {
		$options = array(
			'classname'   => 'status_widget',
			'description' => 'Live status widget. Works with multisite.',
		);
		parent::__construct( 'status_widget', 'Live Status Widget', $options );

		// Enqueue scripts only if widget active.
		if ( is_active_widget( false, false, $this->id_base, true ) ) {
			add_action( 'wp_enqueue_scripts', array( $this, 'widgets_enqueues' ) );
		}
	}

	/**
	 * Outputs the content of the widget.
	 *
	 * @param array $args     Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ) {
		// Add multisite class to widget.
		if ( 'on' === $instance['multisite'] ) {
			$args['before_widget'] = str_replace( 'widget status_widget', 'widget status_widget multisite', $args['before_widget'] );
		}
		echo wp_kses_post( $args['before_widget'] );
		if ( ! empty( $instance['title'] ) ) {
			echo wp_kses_post( $args['before_title'] ) . esc_html( apply_filters( 'widget_title', $instance['title'] ) ) . wp_kses_post( $args['after_title'] );
			echo '<div class="stats-content"></div>';
		}
		echo wp_kses_post( $args['after_widget'] );
	}

	/**
	 * Outputs the options form on admin.
	 *
	 * @param array $instance The widget options.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Website Stats', 'statswidget' );
		$multisite = ! empty( $instance['multisite'] ) ? $instance['multisite'] : 'off';
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'statswidget' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>

		<?php if ( is_multisite() ) { ?>
		<p>
			<input class="checkbox" type="checkbox" <?php checked( $multisite, 'on' ); ?> id="<?php echo esc_attr( $this->get_field_name( 'multisite' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'multisite' ) ); ?>" />
			<label for="<?php echo esc_attr( $this->get_field_name( 'multisite' ) ); ?>"><?php esc_attr_e( 'Show stats for all sites in multisite?', 'statswidget' ); ?></label>
		</p>
		<?php } ?>
		<?php
	}

	/**
	 * Processing widget options on save.
	 *
	 * @param array $new_instance The new options.
	 * @param array $old_instance The previous options.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
		$instance['multisite'] = ( ! empty( $new_instance['multisite'] ) ) ? sanitize_text_field( $new_instance['multisite'] ) : '';
		return $instance;
	}

	/**
	 * Outputs scripts used for the widget.
	 */
	public function widgets_enqueues() {
		wp_enqueue_style( 'stats-widget-styles', Assets_Loader::instance()->get_assets_uri( 'stats-widget.css' ), [], null, true );
		wp_enqueue_script( 'heartbeat' );
		wp_enqueue_script( 'stats-widget-scripts-common', Assets_Loader::instance()->get_assets_uri( 'commons.js' ), [], null, true );
		wp_enqueue_script( 'stats-widget-scripts', Assets_Loader::instance()->get_assets_uri( 'stats-widget.js' ), [], null, true );
		wp_localize_script(
			'stats-widget-scripts',
			'translation',
			array(
				'posts'      => esc_attr__( 'Posts', 'statswidget' ),
				'users'      => esc_attr__( 'Users', 'statswidget' ),
			)
		);
	}
}

/**
 * Register SW_Widget widget.
 */
function register_sw_widget() {
	register_widget( 'SW_Widget' );
}
add_action( 'widgets_init', 'register_sw_widget' );
