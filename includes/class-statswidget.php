<?php
/**
 * StatsWidget setup
 *
 * @package StatsWidget
 */

defined( 'ABSPATH' ) || exit;

/**
 * Main StatsWidget Class.
 *
 * @class StatsWidget
 */
class StatsWidget {
	/**
	 * StatsWidget Constructor.
	 */
	public function __construct() {
		$this->define_constants();
		$this->includes();
	}

	/**
	 * Define SW Constants.
	 */
	private function define_constants() {
		$this->define( 'SW_ABSPATH', dirname( STATS_PLUGIN_FILE ) . '/' );
		$this->define( 'SW_PLUGIN_BASENAME', plugin_basename( STATS_PLUGIN_FILE ) );
		$this->define( 'SW_PLUGIN_URL', plugin_dir_url( STATS_PLUGIN_FILE ) );
		$this->define( 'SW_ENDPOINT_NAME', 'live-stats' );
	}

	/**
	 * Define constant if not already set.
	 *
	 * @param string      $name  Constant name.
	 * @param string|bool $value Constant value.
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * Include required core files used in admin and on the frontend.
	 */
	public function includes() {
		include_once SW_ABSPATH . 'includes/class-assets-loader.php';
		include_once SW_ABSPATH . 'includes/class-sw-widget.php';
		include_once SW_ABSPATH . 'includes/class-sw-api.php';
	}

	/**
	 * Get the plugin url.
	 *
	 * @return string
	 */
	public function plugin_url() {
		return untrailingslashit( plugins_url( '/', STATS_PLUGIN_FILE ) );
	}
}

new StatsWidget();
