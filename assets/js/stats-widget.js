/**
 * Globals
 */
const $ = window.jQuery;

/**
 * Internal variables
 */
const $window = $( window );

/**
 * Window ready
 */
$window.ready( () => {
	updateStats();
});

/**
 * On heartbeat tick
 */
$( document ).on( 'heartbeat-tick', function(e, data) {
	updateStats();
});

function updateStats() {
	let url = '/wp-json/live-stats/v1/stats/singlesite';
	let type = document.querySelector('.status_widget.multisite') ? 'multisite' : 'singlesite';

	if ( 'multisite' === type ) {
		url = '/wp-json/live-stats/v1/stats/multisite'
	}

	$.ajax({
		url: url,
		type: 'GET',
		contentType: 'application/json',
		dataType: 'json',
		async: true,
		success: function ( response ) {
			// Empty data and print new data.
			emptyStats();
			for (let key in response) {
				let obj = response[key];
				printStats( obj )
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log( textStatus + " in getJsonData: " + errorThrown + " " + jqXHR );
		}
	});
}
function emptyStats() {
	let widgets = document.querySelectorAll( '.status_widget .stats-content' );
	widgets.forEach( function( element ) {
		while (element.firstChild) element.removeChild(element.firstChild);
	});
}

function printStats( response ) {
	let widgets = document.querySelectorAll( '.status_widget .stats-content' );
	widgets.forEach( function( element ) {
		// Write new data;
		$( element ).append( '<div class="sw-blog-title">' + response.name + '</div>' );
		$( element ).append( '<div>' + translation.users + ': ' + response.user_count + '</div>' );
		$( element ).append( '<div>' + translation.posts + ': ' + response.posts_count + '</div>' );
	});
}
