<?php
/**
 * StatsWidget API
 *
 * @package StatsWidget/API
 */

defined( 'ABSPATH' ) || exit;

/**
 * StatsWidget API.
 *
 * @class StatsWidget
 */
class SW_API {
	/**
	 * Site ids.
	 *
	 * @var array $site_ids Site ids.
	 */
	public $site_ids;

	/**
	 * StatsWidget API Constructor.
	 */
	public function __construct() {
		$this->set_site_ids( $this->sites_ids() );
		add_action( 'rest_api_init', array( $this, 'register_endpoints' ) );
		$this->invalidate_cache();
	}

	/**
	 * Register stats plugin API endpoints.
	 */
	public function register_endpoints() {
		register_rest_route( SW_ENDPOINT_NAME . '/v1', '/stats/(?P<type>[\w-]+)', array(
			'args' => array(
				'type' => array(
					'description' => __( 'An string identifier for single site or multisite.' ),
					'type'        => 'string',
				),
			),
			'methods' => 'GET',
			'callback' => array( $this, 'return_stats' ),
		) );
	}

	/**
	 * Return stats.
	 *
	 * @param WP_REST_Request $request API request param.
	 *
	 * @return array
	 */
	public function return_stats( $request ) {

		$request_type = $request->get_param( 'type' );
		// Bail fast.
		if ( ! in_array( $request_type, array( 'singlesite', 'multisite' ), true ) ) {
			return;
		}

		$response = array();

		// Single site case.
		if ( 'singlesite' === $request_type ) {
			$blog_id = get_current_blog_id();
			$response[ $blog_id ]['name'] = get_bloginfo( 'name' );
			$response[ $blog_id ]['user_count'] = $this->get_site_user_count( $blog_id );
			$response[ $blog_id ]['posts_count'] = $this->get_site_posts_count( $blog_id );

			return $response;
		}

		// If not multisite bail.
		if ( ! is_multisite() ) {
			return;
		}

		$sites_ids = $this->get_site_ids();
		foreach ( $sites_ids as $blog_id ) {
			$current_blog_details = get_blog_details( array( 'blog_id' => $blog_id ) );
			$response[ $blog_id ]['name'] = $current_blog_details->blogname;
			$response[ $blog_id ]['user_count'] = $this->get_site_user_count( $blog_id );
			$response[ $blog_id ]['posts_count'] = $this->get_site_posts_count( $blog_id );
		}
		return $response;
	}

	/**
	 * Returns sites ids.
	 */
	public function sites_ids() {
		$sites_ids = get_transient( 'ws_sites_ids', true );
		// If no cached response do the query.
		if ( empty( $sites_ids ) ) {
			// Get public sites.
			$args = array(
				'fields' => 'ids',
				'public' => '1',
			);
			// Filter in case it is required to filter the displayed sites for witch the widget shows the stats.
			$sites_ids = is_multisite() ? get_sites( apply_filters( 'ws_get_sites', $args ) ) : array( '0' );
			// Cache sites id's.
			set_site_transient( 'ws_sites_ids', $sites_ids, 24 * 60 * 60 );
		}
		return $sites_ids;
	}

	/**
	 * Returns site user count.
	 *
	 * @param int $site_id Site id.
	 *
	 * @return int
	 */
	public function get_site_user_count( $site_id ) {
		$count_users = get_site_transient( 'ws_user_count_' . $site_id, true );
		// If no transient set for user count on this site id do the extensive CPU query and then cache it for 1 hour.
		if ( empty( $count_users ) ) {
			$users_in_site = count_users( 'time', $site_id );
			$count_users = $users_in_site['total_users'];
			set_site_transient( 'ws_user_count_' . $site_id, $count_users, 60 * 60 );
		}
		return $count_users;
	}

	/**
	 * Get site blog count.
	 *
	 * @param int $site_id Site id.
	 *
	 * @return int
	 */
	public function get_site_posts_count( $site_id ) {
		$count_posts = get_site_transient( 'ws_posts_count_' . $site_id, true );
		// If no transient set for post count on this site id do the query and then cache it for 1 hour.
		if ( empty( $count_posts ) ) {
			if ( is_multisite() ) {
				switch_to_blog( $site_id );
				$count_posts = wp_count_posts( 'post', 'readable' );
				restore_current_blog();
			} else {
				$count_posts = wp_count_posts( 'post', 'readable' );
			}
			set_site_transient( 'ws_posts_count_' . $site_id, $count_posts, 60 * 60 );
		}

		if ( ! empty( $count_posts->publish ) ) {
			return $count_posts->publish;
		}
		return null;
	}

	/**
	 * Invalidate cache.
	 */
	public function invalidate_cache() {
		// Remove blogs ids cache when blog count changes.
		add_action('update_site_option_blog_count', function () {
			remove_site_transient( 'ws_sites_ids' );
		} );

		// Remove user count cache when post updates.
		add_action('register_new_user', function () {
			$this->remove_site_transients( 'ws_user_count', $this->get_site_ids() );
		} );

		// Remove user count cache when post updates.
		add_action('remove_user_from_blog', function () {
			$this->remove_site_transients( 'ws_user_count', $this->get_site_ids() );
		} );

		// Remove blog posts count cache when post updates.
		add_action('save_post', function () {
			$this->remove_site_transients( 'ws_posts_count', $this->get_site_ids() );
		} );
	}

	/**
	 * Removes recursively the transients from a site network;
	 *
	 * @param string $transient Transient name without trailing underscore.
	 * @param array  $site_ids  Site ids array.
	 */
	public function remove_site_transients( $transient, $site_ids ) {
		if ( empty( $transient ) || empty( $site_ids ) ) {
			return;
		}

		foreach ( $site_ids as $site_id ) {
			delete_site_transient( $transient . '_' . $site_id );
		}
	}

	/**
	 * Get $site_ids var.
	 *
	 * @return array $this->site_ids Array of site ids.
	 */
	public function get_site_ids() {
		return $this->site_ids;
	}

	/**
	 * Set $site_ids var.
	 *
	 * @param array $site_ids Array of site ids.
	 */
	public function set_site_ids( $site_ids ) {
		$this->site_ids = $site_ids;
	}
}

new SW_API();
