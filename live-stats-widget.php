<?php
/**
 * Plugin Name: Live Stats Widget
 * Plugin URI: https://bogdandragomir.com/stats-widget
 * Description: A stats widget that shows number of posts, users, pages, categories, etc.
 * Version: 0.1.0
 * Author: Bogdan Dragomir
 * Author URI: https://bogdandragomir.com
 * Text Domain: statswidget
 * Domain Path: /languages/
 *
 * @package StatsWidget
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define STATS_PLUGIN_FILE.
if ( ! defined( 'STATS_PLUGIN_FILE' ) ) {
	define( 'STATS_PLUGIN_FILE', __FILE__ );
}

// Include the main StatsWidget class.
if ( ! class_exists( 'StatsWidget' ) ) {
	include_once dirname( __FILE__ ) . '/includes/class-statswidget.php';
}
